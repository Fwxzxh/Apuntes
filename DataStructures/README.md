# Estructuras de datos

## Índice
* [Estructuras de datos y Big-O](DataStructures.md)
* [Arrays dinámicos y estáticos](Arrays.md)
* [Linked lists](LinkedLists.md)
* [Stacks](Stack.md)
* [Queues](Queues.md)
* [Priority Queues](PriorityQueues.md)
* [Union Find](UnionFind.md)
* 
