# Funciones

[[_TOC-]]

Cuando se ejecuta un programa en C++ la primera función en ejecutarse es la 
función `main`.

## Argumentos

A veces las funciones pueden venir en una forma similar a esta:
`pow()`, dentro de esos parentesis, van unas cosas llamadas argumentos que pueden 
ser uno o más, por ejemplo `pow(10,2);`. 

Estas funciones normalmente retornan algún valor, el cual se puede ser usado en una
expreción más grande. 

```c++
x = pow(10,2) * 2;
```

## Creando Funciones

Las funciones son útiles cuando hacemos cierto procedimiento reiteradas veces, 
en vez de escribir ese código cada vez que queramos hacer ese procedimiento solo 
llamamos a la función con los argumentos que necesita.
Y llamamos a esta función con un nombre(llamado identificador).

En una función podemos tener unos parentesis `()`,  en estos es donde tenemos el 
input de la función y guardaremos ese input en algo llamado parametros.

Entonces Cuando llamamos una función le pasamos argumentos, los cuales son guardados
en los parametros.


Entonces podemos Crear nuesta primera función:

```c++
int multiply(int x, int y)
{
	return x*y;
}

int multiply(int,int); 	//declaration

int x = 5;
x = multiply(x,5); //declaration, definición de función, calling

```


## Usando funciones

```c++
#include <iostream>
#include <cmath> 

using std::cout;
using std::cin;

int main()
{
	int base, exponent;
	cout << "what is the base?:"
	cin >> base;
	cout << "what is the exponent";
	cin >> exponent;
	double power << pow(10,2); //pow viene de la libreria cmath
	cout << power << endl;
}
```

