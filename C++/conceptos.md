# Conceptos básicos

[[_TOC_]]

## Nuestro primer hello world

```c++
#include <iostream>

int main() //main function
{
	std::cout << "Hello World\n";
	return 0;
}	
```

Para compilar esto tecleamos el comando `g++ nombre_del_archivo.cpp`

Como resultado tenemos un archivo de nombre `a.out` que es el resultado de nuestra
compilación, lo ejecutamos con el comando `./a.out` y tendremos un `Hello world`
en la consola.

## Función Main

Dado el sig programa.

```c++
int main() //función main
{
	return 0;
}
```

`int main()` esta es la función `main`, podemos pensar que una función es una 
máquina que hace _algo_, a veces puede que tenga uno o más _imputs_ o _outputs_.

Lo primero que hace un programa de c++ es llamar a la función `main`, encendiendo 
nuestra máquina.

`return 0;` es el _output_ de nuestra función `main`, literalmente dice "el output es 
0", el 0 significa para la computadora "he terminado el proceso, todo fue bien". 
íunque podemos eliminar esta ultima línea y estara implícita.

En nustro ejemplo de Hello World, escribimos en la consola texto con el comando 
`std::cout << "Hello World\n";` esta herramienta no esta disponible desde el principio,
la incluimos en nuestro programa con la sentencia `#include <iostream>`, `iostream` es 
literalmente _imput output stream_, esto funciona debido a algo llamado Preprocesamiento,
esto ocurre antes de que el código sea compilado, busca el archivo llamado `iostream` 
y lo pone dentro de nuestro programa para que podamos ocupar las herramientas que están
en ese archivo.

El `int` en `int main()` es el tipo de retorno, indica que tipo de dato va a ser retornado

`std` en `std::cout << "hello World\n";` es lo que le le llama un _namespace_, que es 
básicamente un grupo de código, en que cada grupo es un _namespace_, nos permite entre 
otras cosas por que si tenemos dos funcciones que se llaman igual, las podemos diferenciar
con su _namespace_, en el ejemplo estamos sacando la función `cout` de el _namespace_ 
`std` que significa _standard_.

Podriamos hacer también algo como esto para definirlo desde el principio.

```c++
using namespace std;
cout << "hello world";
```
> Esto puede ser considerado mala páctica, por que puede haber conflictos de nombre, pero 
> está bien en programas cortos.

## POO

En la _Programación Orientada a Objetos_ tenemos un esquema en el que todo se deriva de las
clases.
```
					---------
					| Clase |
					---------
					/   |   \
				       O    O    O
```
Cada clase da a lugar a uno o varios objetos, (denotados por la O), podemos pensar en la 
clase como el molde de una galleta, y los objetos como las galletas que salen del molde,
estas comparten la forma y el tamaño el cúal fue dado por el molde.

El ejemplo usado en nuestro ejemplo es `cout`, el cúal es un objeto de la clase `iostream`

## Operadores 
En nuestro ejemplo usmos un operador, que sería el `<<` en `std::cout << "hello World";`
en este caso lo que significa es "toma este string ("hello world") y mandalo a el objeto
`cout`.


## Directivas y declaraciones  

### Directivas 
En el Capitulo anterior teniamos algo como esto:

```c++
#include <iostream>

using namespace std; //Directiva

int main() //main function
{
	cout << "Hello World\n";
//	std::cout << "Hello World\n"; la directiva nos evita escribirlo de esta forma
	return 0;
}	
```

La directiva nos dice de que _namespace_ vamos a sacar ciertas objetos, en este 
caso `cout` viene de la directiva `std` y con poner la directiva de std no tenemos que
llamarla cada vez que usemos `cout`, esto puede llevar a problemas con los nombres de 
ciertas funciones, por lo tanto se recomienda usarlas solo en programas pequeños.

Hay una solución que es un punto medio en este asunto, el cual seria hacer una declaración

```c++
#include <iostream>

using std::cout;

int main() //main function
{
	cout << "Hello World\n";
	return 0;
}	
```

De esta manera solo declaramos el elemento que queremos usar, en este caso `cout`, 
esto agrega flexibidad ya que podemos hacer algo como esto.

```c++
#include <iostream>


int main() //main function
{
	using std::cout;
	cout << "Hello World\n";
	return 0;
}	
```

De esta manera podemos declarar `cout` solo para la función `main`, al contrario de 
ponerlo afuera, el cual lo declara para todo el archivo.


## Variables

## Declaración e Inicialización
Las variables nos permiten guardar información para despues ocuparla en nuestro 
programa. 

```c++
#include <iostream>

using std::cout;

int main() //main function
{
	int slices = 5; 
	slices = 5 + 1;
	int children = slices;
	slices = 1000;
	return 0;
}	
```

`int slices = 5` Aquí declaramos una variable llamada _slices_ con valor de 5 y de tipo `int`
fueron llevados dos fases, la declaración y la inicialización, aqui lo estamos haciendo
todo en una línea, pero puede hacerse de manera separada. 

### Concatenación de strings y output 

```c++
#include <iostream>

using std::cout;

int main() //main function
{
	int slices = 5; 
	cout << "You Have" << slices << "Slices of Pizza." << std::endl; 
	return 0;
// printf es una función que viene de C, $i viene de integer, y \n es el salto de linea
	printf("%i\n", slices); 
}	
```

### Input

El 5 de cada `slices` de pizza esta _HardCoded_ en el código, lo cual es un problema
ya que cada que queramos cambiar ese valor tenemos que cambiar el código fuente.

Así que le vamos a pedir al usario que nos de el valor de el número.

```c++
#include <iostream>

using std::cout;
using std::cin;

int main() //main function
{
	int slices; 
	cin >> slices; // console in
	cout << "You Have" << slices << "Slices of Pizza." << std::endl; 
}	
```

## Convenciones y Guías de estilo

Para más información: [Core guidelines](https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md)

```c++
#include <iostream>

using std::cout;
using std::cin;

int main() //main function
{
	// Cada vez que tengamos código ente llaves `{}` este se va a indentar con un TAB
	//mantener los comentarios al mínimo y consisos
	//preferir Variables_separadas_con_guiones, sobre cualquer otro tipo
	int SLICES;
	int SliCes;
	int slices; // SLICES != slices, pero se debe seguir una convención fija

	/*
	comentarios multi linea
	asdfasfd
	*/

	cout << "Y0 fatty how many pieces of pizza you eat?: ";
	cin >> slices; 
	cout << "You Have" << slices << "Slices of Pizza." << std::endl; 
}	
```

