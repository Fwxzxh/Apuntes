# Datos personales

**Índice**

[[_TOC_]]

## ¿Qué es la ciberseguridad?
Es el esfuerzo constante por proteger los sistemas informaticos y sus datos contra su uso
no autorizado.

_Se debe proteger.._

* **A nivel Personal:**
	* Identidad.
	* Datos
	* Dispositivos informáticos.
* **A nivel Corporativo;**
	* La reputación.
	* Los datos.
	* Clientes.
* **A nivel de estado:**
	* Seguridad nacional.
	* Seguridad y bienestar de los ciudadanos.

## Identidad en linea y fuera de linea
Entre más tiempo se pasa en linea su identidad en linea y fuera de, pueden afectar tu vida.

**Identidad fuera de linea**

La persona que eres con tus amigos, familiares, en el hogar, escuela o trabajo. Conocen tu información personal:
* Nombre, edad, dirección etc.

**Identidad en linea**

Como te presentas a otros en linea.
* Solo deberia revelar una cantidad limitada de información.

### Nickames
_No debe contener..._
* Información personal.
* Correcto y respetuoso.
No debe llevar a extaños a pensar que es un objetivo fácil para delitos ciberneticos o llamar
a atención no deseada.

## Datos
Cualquer información sobre used puede ser considerada como sus datos y puede identificarlo de
manera única como persona.

_estos datos incluyen_
* Imagenes.
* Mensajes que intercambia con amigos y conocidos en linea.
* Nombre.
* Número de seguro social.
* Fecha y lugar de nacimiento.
* Informaión médica, educativa, financiera y laboral.

<p align=Center>
	<img src="Imagenes/Cap-1/Img-1.png">
</p>

### Expediente médico 
Cada vez que uno asiste al consultorio médico, esa información se agrega a su hisporial médico.
Este incluye cosas como:
* Estado físico y mental.
* Información familiar.

### Historial educativo
Es la información sobre sus notas y puntajes en las evaluaciónes asi como reconocimientos y títulos
adquiridos. Tambíen puede incluir información sde contacto, salud y su historial de inmunización.

### Historial financiero
Incluye información sobre sus ingresos y gastos.

El historial de impuestos puede incluir:
* Talones de cheques de pago
* Resumenes de tarjetas de crédito.
* Calificación crediticia.
* Información sobre sus pasados empleos y su rendimiento.

## ¿Dónde están sus datos?
Existen distintas leyes que protejen la privacidad y los datos en cada país. 
pero ¿Dónde están sus datos?

La información registrada en su expediente médico se registra para fines de facturación, y esta
información se puede compartir con la empresa de seguros para garantizar la facturación y 
la calidad adecuadas. Ahora, una parte de su historial medico también se encuentra en la empresa
de seguros.

Las tarjetas de fidelidad de la tienda pueden ser una manera conveniente de ahorrar dinero 
en sus compras. Sin embargo, la tienda compila un perfil de sus compras y utiliza esa información
para su propio uso.

El perfil muestra que un comprador compra cierta marca y sabor de crema dental regularmente.
La tienda utiliza esta información para identificar como objetivo al comprador con ofertas
especiales del partner de marketing. Con esto ellos tienen un perfil del comportamiento de un cliente.

### Sus dispositivos informáticos
Estos no solo almacenan datos, Si no también se han convertido en el portar a sus datos 
y generan información sobre usted.

A menos de que haya seleccionado recibir los resúmenes en papel para todas sus cuentas,
seguramente utiliza sus dispositivos informáticos para acceder a estos datos, los cuales 
pueden ser...
* El último resumen de su tarjeta de crédito
* Pagar su factura
* Acceder a la pagina web de su banco para transferir los fondos desde su celular

Con toda esa información disponible en linea, sus datos personales se han vuelto rentables para
los hackers.

