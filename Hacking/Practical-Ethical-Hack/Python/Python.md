# Python

[[_TOC_]]

## Shebangs y Strings
Siempre que hagamos un script en python tenemos que incluir una _shebang_ al inicio 
del script, esta le dira al script la dirección del interprete de python.

```python
#! /bin/python #shebang

#Print hello world
print("hello, world!")
print('hello, world!')
print('\n') # salto de linea
print("""hellooo, 
world""") # hacky multiline string

print("This string is " + "awesome") #conatenation
```

De esta manera podremos ejecutarlo haciendo `./nombre_del_script.py`

## Math

Python es muy bueno para trabajar con números.

```python
#! /bin/python 

print(50 + 50) #add
print(50 - 50) #substract
print(50 * 50) #multiply
print(50 / 50) #divide
print(50 ** 50) #50^50
print(50 % 50) #modulo
print(50 // 50) #div sin decimales

```

## Variables y métodos

```python
#! /bin/python 

#variables y métodos
quote = "All is fair in love and war." # variable
print(quote.upper()) # hacemos el string mayus con un el método upper()

print(len(quote))

name = "Health" #string str()
age = 30 # integer int()
gpa = 3.7 # float float()

print(int(gpa)) # con esto quitaremos los decimales de la variable gpa haciendolo un integer 

print("My name is "+ name +"and I am "+ str(age) +"years old.") #solo podremos concatenar strings

age += 1 # age + 1, equivalente a `age = age + 1`

```

## Funciones
Como pequeños trozos de código que podemos llamar cuando necesitemos. 
```python

def who_am_i(): # definimos una función
	name = "Health"
	age = 30
	gpa = 3.7
	print("My name is "+ name +"and I am "+ str(age) +"years old.")

who_am_i() # llamamos a la función

#añadiendo parámetros
def add_one_hundred(num):
	print(num + 100)

add_one_hundred(100)

#múltiples parámetros 
def add(x,y):
	print(x+y)

add(7,7)


def multiply(x,y):
	return (x*y) # aquí retornamos el resultado, para que otra función o método pueda usarlo

print(multiply(7,7)

```

## Booleans

```python
bool1 = True
bool2 = 3*3 == 9 #True
bool3 = False
bool4 = 3*3 != 9

print(bool1, bool2, bool3, bool4)
# True True False False 
print(type(bool1))
# <class 'bool'> 

```

## Operadores Relacionales y booleanos

```python
greater_than = 7 > 		#all true
less_than = 5 < 7
greater_than_equal + 7 >= 7
less_than_equal_to 7 <= 7

test_and = (7 > 5) and (5 < 7)  #true
test_and2 = (7 > 5) and (5 > 7)  #false
test_or = (7 > 5) or (5 < 7)  #true
test_or2 = (7 > 5) or (5 > 7)  #true

test_not = not True #false
```

## Condicionales

```python
def drink(money):
	if money >= 2:
		return "You've got yourself a drink"
	else:
		return "NO drink for you"

def alcohol(age,money):
	if (age >= 21) and (money >= 5):
		return "We're getting a drink!"
	elif (age >= 21) and (money < 5):
		return "Come back with more money"
	elif (age < 21) and (money >= 5):
		return "Nice Try, kid!"
	else:
		return "You're too poor and too young"
print(drink(3))
print(alcohol(21,5))
```

## listas
Listas mutables
```python
#list - have brackets []
movies = ["When Harry Met Sally","The Hangover","The Perks of Being a Wallflower","The exorcist"]

print(movies[0]) # el primer elemento es el 0
print(movies[1:3]) # los items del 1 al 3 
print(movies[1:]) # los items del 1 al último 
print(movies[:2]) # del primero hasta el 2
print(movies[-1]) # El último item
print(len(movies)) # la longitud de la lisa
movies.append("JAWS")# agregar elementos a la lista
movies.pop() #Retorna y elimina el último elemento de la lista
```

## Tuples
Listas no mutables
```python
#Tuples - Do not change, ()
grades=("a","b","c","d","f")
print(grades[1]))

```

## Importing Modules

```python
#! /bin/python 
import sys # para funciones del sistema (importante)
import os  #
from datetime import datetime #podemos importar solo un metodo
from datetime import datetime as dt # o lo podemos importar con un alias.

print(sys.version)
print(datetime.now())
print(dt.now()) # usndo el alias
```

# Strings avanzados

```python
my_name = "Health"
print(my_name[0]) # imprimimos solo la primera letra
print(my_name[-1]) # imprimimos solo la última letra

sentence = "This is a sentence"
print(sentence[:4])

sentence_join = sentence.split(" ")# separamos el string en cada " "

sentence_join = ' '.join(sentence_join)# unimos la oración de nuevo
print(sentence_join)

quote = "he said, \"give me all your money\"" # \ hace que ignore los carateres
print(quote)

too_much_space="        	hello     	"
print(too_much_space.strip())# quita los espacios, el " " viene inplicito, le posemos dar cualquier arg

print("a" in "Apple") # FALSE

letter = "A"
word = "Apple"
print(letter.lower() in world.lower())# improved

movie="The Hangover"
print("My favorite movie is "+movie+".")
print("My favorite movie is {}.".format(movie))#usando un placeholder

```

## Diccionarios

```python
#Diccionarios - key/value {}
drink={"White Russian":7, "Old Fashion": 10, "Lemon Drop": 8}#drink=key price=value
print(drink)
employees={"Finance":["bob","Linda","Tina"], "IT":["Gene","Louse","Teddy"], "HR":["Jimmy","Mort"]}
print(employees)

employees["Legal"] = ["Mr. Frond"]# add a new key: value pair
print(employees)

employees.update({"Sales": ["Andie","Ollie"]#same
print(employees)

drink["White Russian"] = 8

print(drink.get("White Russian")

```

## Sokets

